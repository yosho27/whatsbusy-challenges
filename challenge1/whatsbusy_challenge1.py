'''
Given a string, return a run length encoding
if it's shorter than the input
'''
def compress(word):
    last_char = word[0]
    count = 1
    result = ''
    #Each time the run changes, append the last one to the result
    for new_char in word[1:]:
        if new_char == last_char:
            count += 1
        else:
            result += last_char + str(count)
            count = 1
            last_char = new_char
    #Append the final run to the result
    result += last_char + str(count)
    #Only return the encoding if it's shorter
    if len(result) < len(word):
        return result
    else:
        return word

'''
A graph is represented by a dictionary
Each key is a node label
Each value is a set of adjacent node labels
'''

'''
Given a chain of nodes formatted like the test cases,
return a dictionary representing the directed graph
'''
def chain2graph(chain):
    chain = chain.split(' -> ')
    graph = {}
    for k in range(len(chain)):
        if not chain[k] in graph:
            graph[chain[k]] = set()
        if k < len(chain)-1:
            graph[chain[k]].add(chain[k+1])
    return graph


'''
Given a dictionary represeting a directed graph,
return a list of nodes with the most connections
If there isn't a tie, the list will be length 1
'''
def identify_router(graph):
    #Create a dictionary to count the links
    links = {key:0 for key in graph.keys()}
    for key in graph:
        #Count outbound links
        links[key] += len(graph[key])
        #Count inbound links
        for adj in graph[key]:
            links[adj] += 1
    #Find the maximum(s)
    max_links = -1
    max_keys = []
    for key in links:
        if links[key] > max_links:
            max_links = links[key]
            max_keys = [key]
        elif links[key] == max_links:
            max_keys.append(key)
    return max_keys
